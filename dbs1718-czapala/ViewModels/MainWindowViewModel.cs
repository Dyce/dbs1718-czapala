﻿using Prism.Mvvm;

namespace dbs1718_czapala.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "DBS Projekt";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public MainWindowViewModel()
        {

        }
    }
}
