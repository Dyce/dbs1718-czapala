﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dbs1718_czapala.ViewModels
{
    public class TopMenuViewModel : BindableBase
    {
        public TopMenuViewModel()
        {

        }

        private DelegateCommand _addMoviesCommand;
        public DelegateCommand AddMoviesCommand =>
            _addMoviesCommand ?? (_addMoviesCommand = new DelegateCommand(ExecuteAddMoviesCommand, CanExecuteAddMoviesCommand));

        void ExecuteAddMoviesCommand()
        {

        }

        bool CanExecuteAddMoviesCommand() => true;

        private DelegateCommand _viewMovieListCommand;
        public DelegateCommand ViewMovieListCommand =>
            _viewMovieListCommand ?? (_viewMovieListCommand = new DelegateCommand(ExecuteViewMovieListCommand, CanExecuteViewMovieListCommand));

        void ExecuteViewMovieListCommand()
        {

        }

        bool CanExecuteViewMovieListCommand() => true;
    }
}
