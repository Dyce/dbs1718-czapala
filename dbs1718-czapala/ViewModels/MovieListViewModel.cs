﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Regions;
using System.Collections.ObjectModel;
using dbs1718_czapala.Model;
using System.Data.Entity;
using System.Runtime.CompilerServices;

namespace dbs1718_czapala.ViewModels
{
    /// <summary>
    /// ViewModel, ktory obsluhuje nahlad zoznamu filmov
    /// </summary>
    public class MovieListViewModel : BindableBase, INavigationAware
    {
        /// <summary>
        /// Konstruktor triedy, nastavuje defaultne hodnoty
        /// </summary>
        /// <param name="regionManager">Interface pre pracu s UI aplikacie</param>
        public MovieListViewModel(IRegionManager regionManager)
        {
            ReleasedMin = DateTime.Parse("1980-01-01").Date;
            ReleasedMax = DateTime.Now.Date;
            RuntimeMin = 0;
            RuntimeMax = 500;
            PageNumber = 1;
            Movies = new ObservableCollection<MovieViewTable>();
            RowsPerPage = 50;
            _regionManager = regionManager;
        }

        private IRegionManager _regionManager;


        //skupina eventhandlerov, ktore spracovavaju vstup od pouzivatela
        #region Commands
        private DelegateCommand _mouseDoubleClickCommand;
        public DelegateCommand MouseDoubleClickCommand =>
            _mouseDoubleClickCommand ?? (_mouseDoubleClickCommand = new DelegateCommand(ExecuteMouseDoubleClickCommand, CanExecuteMouseDoubleClickCommand));

        void ExecuteMouseDoubleClickCommand()
        {
            System.Windows.MessageBox.Show("CLICK");
        }

        bool CanExecuteMouseDoubleClickCommand()
        {
            return SelectedMovie != null;
        }

        private DelegateCommand _firstPageCommand;
        public DelegateCommand FirstPageCommand =>
            _firstPageCommand ?? (_firstPageCommand = new DelegateCommand(ExecuteFirstPageCommand, CanExecuteFirstPageCommand))
            .ObservesProperty(() => Movies);

        void ExecuteFirstPageCommand()
        {
            GetMovies(0);
        }

        bool CanExecuteFirstPageCommand()
        {
            return Movies.Count != 0;
        }

        private DelegateCommand _lastPageCommand;
        public DelegateCommand LastPageCommand =>
            _lastPageCommand ?? (_lastPageCommand = new DelegateCommand(ExecuteLastPageCommand, CanExecuteLastPageCommand))
            .ObservesProperty(() => Movies);

        void ExecuteLastPageCommand()
        {
            GetMovies(0, true);
        }

        bool CanExecuteLastPageCommand()
        {
            return Movies.Count != 0;
        }

        private DelegateCommand _previousPageCommand;
        public DelegateCommand PreviousPageCommand =>
            _previousPageCommand ?? (_previousPageCommand = new DelegateCommand(ExecutePreviousPageCommand, CanExecutePreviousPageCommand))
            .ObservesProperty(() => Movies);

        void ExecutePreviousPageCommand()
        {
            GetMovies(Movies.OrderBy(m => m.MovieId).First().MovieId, true);
        }

        bool CanExecutePreviousPageCommand()
        {
            return Movies.Count != 0;
        }


        private DelegateCommand _nextPageCommand;
        public DelegateCommand NextPageCommand =>
            _nextPageCommand ?? (_nextPageCommand = new DelegateCommand(ExecuteNextPageCommand, CanExecuteNextPageCommand))
            .ObservesProperty(() => Movies);

        private bool CanExecuteNextPageCommand()
        {
            return Movies.Count != 0;
        }

        void ExecuteNextPageCommand()
        {
            GetMovies(Movies.OrderBy(m => m.MovieId).Last().MovieId);
        }

        private DelegateCommand _executeQueryCommand;
        public DelegateCommand ExecuteQueryCommand =>
            _executeQueryCommand ?? (_executeQueryCommand = new DelegateCommand(ExecuteExecuteQueryCommand, CanExecuteExecuteQueryCommand));

        void ExecuteExecuteQueryCommand()
        {
            //int lastId = Movies.OrderBy(m => m.MovieId).Last().MovieId;
            //Movies.Clear();
            GetMovies(lastId: 0);
        }

        bool CanExecuteExecuteQueryCommand()
        {
            return true;
        }
        #endregion


        //skupina premennych, na ktore sa viazu UI elementy
 #region BindableProperties
        private int _rowsPerPage;
        public int RowsPerPage
        {
            get { return _rowsPerPage; }
            set { SetProperty(ref _rowsPerPage, value); }
        }

        private DateTime _releasedMin;
        public DateTime ReleasedMin
        {
            get { return _releasedMin; }
            set { SetProperty(ref _releasedMin, value); }
        }

        private DateTime _releasedMax;
        public DateTime ReleasedMax
        {
            get { return _releasedMax; }
            set { SetProperty(ref _releasedMax, value); }
        }

        private int _productionCompanyId;
        public int ProductionCompanyId
        {
            get { return _productionCompanyId; }
            set { SetProperty(ref _productionCompanyId, value); }
        }

        private ObservableCollection<MovieViewTable> _movies;
        public ObservableCollection<MovieViewTable> Movies
        {
            get { return _movies; }
            set { SetProperty(ref _movies, value); }
        }

        private MovieViewTable _selectedMovie;
        public MovieViewTable SelectedMovie
        {
            get { return _selectedMovie; }
            set { SetProperty(ref _selectedMovie, value); }
        }

        private int _runtimeMin;
        public int RuntimeMin
        {
            get { return _runtimeMin; }
            set { SetProperty(ref _runtimeMin, value); }
        }
        
        private int _runtimeMax;
        public int RuntimeMax
        {
            get { return _runtimeMax; }
            set { SetProperty(ref _runtimeMax, value); }
        }

        private int _pageNumber;
        public int PageNumber
        {
            get { return _pageNumber; }
            set { SetProperty(ref _pageNumber, value); }
        }

        private string _movieName;
        public string MovieName
        {
            get { return _movieName; }
            set { SetProperty(ref _movieName, value); }
        }
        
        private bool _includeNulls;
        public bool IncludeNulls
        {
            get { return _includeNulls; }
            set
            {
                Console.WriteLine($"Setting property to: {value}");
                SetProperty(ref _includeNulls, value);
            }
        }
#endregion

        /// <summary>
        /// Funkcia, ktora zabezpecuje komunikaciu s databazou
        /// </summary>
        /// <param name="lastId">ID posledneho aktualne zobrazeneho filmu</param>
        /// <param name="inverseOrder">Obrati poradie riadkov</param>
        /// <param name="callerMethod">Meno volajucej funkcie</param>
        private void GetMovies(int lastId, bool inverseOrder = false , [CallerMemberName] string callerMethod = "")
        {
            //trieda pre pracu s databazou
            using (DBSContext context = new DBSContext())
            {
                //query, ktora sa posle do databazy, postupne sa bude skladat
                IQueryable<Movie> movies;

                //ak tuto funkciu zavolal event handler pre zobrazenie predchadzajucej strany, implicitne musi byt inverse order true
                inverseOrder = string.Equals(callerMethod, nameof(ExecuteLastPageCommand)) ? true : inverseOrder;

                //spracovanie inverse order parametra
                if (inverseOrder)
                    movies = context.Movies.OrderByDescending(m => m.Id);
                else 
                    movies = context.Movies.OrderBy(m => m.Id);

                //maju byt vo vysledku zahrnute aj riadky s hodnotou null?
                if (IncludeNulls)
                    movies = movies.Where(m => (m.Released >= ReleasedMin && m.Released <= ReleasedMax) || m.Released == null)
                          .Where(m => (m.Runtime >= RuntimeMin && m.Runtime <= RuntimeMax) || m.Runtime == null);
                else
                    movies = movies.Where(m => m.Released >= ReleasedMin && m.Released <= ReleasedMax)
                          .Where(m => m.Runtime >= RuntimeMin && m.Runtime <= RuntimeMax);

                //ak bolo zadane meno, filtrujeme vysledky aj podla mena filmu
                if (!string.IsNullOrWhiteSpace(MovieName))
                    movies = movies.Where(m => m.Name.Contains(MovieName));

                //ak nie je ID spolocnosti 0, hladame filmy, ktore ma pod palcom spolocnost s danym ID (neskor bude upravene na filtrovanie podla mena)
                if (ProductionCompanyId != 0)
                    movies = movies.Where(m => m.ProductionCompanyId == ProductionCompanyId);

                //ak funkciu zavolal event handler pre predchadzajucu stranu, treba otocit porovnavanie
                if (callerMethod == nameof(ExecutePreviousPageCommand))
                    movies = movies.Where(m => m.Id < lastId);
                else
                    movies = movies.Where(m => m.Id > lastId);

                //pomocny vypis aktualnej (nie konecnej) query do konzoly
                Console.WriteLine(movies.ToString());

                //sem sa ulozia vysledne riadky, tu sa joinuju dve dalsie tabulky a vybera sa, ktore stlpce budu vo vysledku zobrazene
                List<MovieViewTable> records = movies.Include(m => m.ProductionCompany).Include(m => m.Languages).Take(50)
                    .Select(m => new MovieViewTable
                    {
                        MovieId = m.Id,
                        MoveName = m.Name,
                        Released = m.Released,
                        Runtime = m.Runtime,
                        Summary = m.Summary,
                        ProductionCompany = m.ProductionCompany.Name,
                        Comments = m.Comments.Count
                    }).OrderBy(m => m.MovieId).ToList();

                //ak query vratila 0 riadkov
                if(records.Count == 0)
                {
                    System.Windows.MessageBox.Show("No records found", "Query result", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
                    return;
                }

                //ak query nasla vyhovujuce riadky, zmazeme aktualne zobrazene riadky...
                Movies.Clear();
                
                //...a zobrazime riadky vratene dopytom
                foreach (MovieViewTable movie in records)
                {
                    Movies.Add(movie);
                }
            }
            //sluzi na aktualizaciu UI
            RaisePropertyChanged(nameof(Movies));
        }


        #region NavigationInterface
        public bool IsNavigationTarget(NavigationContext navigationContext) => true;

        public void OnNavigatedFrom(NavigationContext navigationContext) { }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            //GetMovies();
        }
        #endregion

    }
}
