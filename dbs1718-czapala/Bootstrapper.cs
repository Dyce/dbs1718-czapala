﻿using dbs1718_czapala.Views;
using dbs1718_czapala.ViewModels;
using System.Windows;
using Prism.Modularity;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Prism.Regions;
using System.Globalization;
using System.Linq;

namespace dbs1718_czapala
{
    class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow.Show();
            //InitializeDatabase();

            IRegionManager regionManager = (IRegionManager)Container.Resolve(typeof(IRegionManager));
            regionManager.RegisterViewWithRegion("TopMenuRegion", typeof(TopMenuView));
            regionManager.RequestNavigate("ContentRegion", nameof(MovieListView));
        }

        protected override void ConfigureModuleCatalog()
        {
            var moduleCatalog = (ModuleCatalog)ModuleCatalog;
            
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            Container.RegisterType<TopMenuView>(new ContainerControlledLifetimeManager()); //singleton

            Container.RegisterTypeForNavigation<MovieListView>(nameof(MovieListView));
        }

        private void InitializeDatabase()
        {
            //using (Model.DBSContext ctx = new Model.DBSContext())
            //{
            //    ctx.Database.Log = System.Console.Write;
            //    ctx.Database.CreateIfNotExists();
            //}

            //CultureInfo[] ci =
            //    CultureInfo.GetCultures(CultureTypes.AllCultures &
            //    ~CultureTypes.NeutralCultures).ToArray();

            //using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"C:\Users\Michal\Desktop\languages.csv", false, System.Text.Encoding.UTF8))
            //{
            //    foreach (CultureInfo culture in ci)
            //    {
            //        sw.WriteLine($"{culture.NativeName},{culture.EnglishName}");
            //    }
            //}


        }
    }
}
