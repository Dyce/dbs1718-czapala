﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dbs1718_czapala.Model
{
    [Table("ratings")]
   public class Rating
    {
        [Key]
        [Column("movie_id", Order = 0)]
        public int MovieId { get; set; }

        [ForeignKey(nameof(MovieId))]
        public virtual Movie Movie { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        [Key]
        [Column("user_id", Order = 1)]
        public int UserId { get; set; }

        [Required]
        [Column("rating")]
        public int Value { get; set; }
    }
}
