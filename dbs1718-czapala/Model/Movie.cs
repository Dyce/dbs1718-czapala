﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dbs1718_czapala.Model
{
    [Table("movies")]
    public class Movie
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column("name")]
        public string Name { get; set; }

        [Column("released")]
        public DateTime? Released { get; set; }

        [Column("runtime")]
        public int? Runtime { get; set; }

        [Column("summary", TypeName = "text")]
        public string Summary { get; set; }

        [Column("production_company_id")]
        [ForeignKey(nameof(ProductionCompany))]
        public int ProductionCompanyId { get; set; }
        public ProductionCompany ProductionCompany { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Language> Languages { get; set; }

        public virtual ICollection<Genre> Genres { get; set; }
    }
}
