namespace dbs1718_czapala.Model
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DBSContext : DbContext
    {
        // Your context has been configured to use a 'DBSContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'dbs1718_czapala.Model.DBSContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'DBSContext' 
        // connection string in the application configuration file.
        public DBSContext() : base("name=DBSContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DBSContext>());
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<ProductionCompany> ProductionCompanies { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<MovieRole> MovieRoles { get; set; }
        public DbSet<Rating> Ratings { get; set; }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasMany(t => t.Genres).WithMany(t => t.Movies).Map(jt =>
            {
                jt.MapLeftKey("MovieId");
                jt.MapRightKey("GenreId");
                jt.ToTable("movies_genres");
            });

            modelBuilder.Entity<Movie>().HasMany(t => t.Languages).WithMany(t => t.Movies).Map(jt =>
            {
                jt.MapLeftKey("MovieId");
                jt.MapRightKey("LanguageId");
                jt.ToTable("movies_languages");
            });

        }


        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}