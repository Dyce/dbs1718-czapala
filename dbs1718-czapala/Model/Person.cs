﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dbs1718_czapala.Model
{
    [Table("people")]
    public class Person
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("first_name")]
        public string FirstName { get; set; }

        [Required]
        [Column("last_name")]
        public string LastName { get; set; }

        [Column("birthday")]
        public DateTime? Birthday { get; set; }

        [Required]
        [Column("gender")]
        public short Gender { get; set; }
    }
}
