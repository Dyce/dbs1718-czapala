﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dbs1718_czapala.Model
{
    [Table("laguages")]
    public class Language
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("name")]
        public string Name { get; set; }

        [Required]
        [Column("english_name")]
        public string EnglishName { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }

    }
}
