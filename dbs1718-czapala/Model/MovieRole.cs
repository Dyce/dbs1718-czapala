﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dbs1718_czapala.Model
{
    [Table("roles_movies")]
    public class MovieRole
    {
        [Key]
        [Column("movie_id", Order = 0)]
        public int MovieId { get; set; }

        [Key]
        [Column("person_id", Order = 1)]
        public int PersonId { get; set; }

        [Key]
        [Column("role_id", Order = 2)]
        public int RoleId { get; set; }

        [ForeignKey(nameof(MovieId))]
        public Movie Movie { get; set; }

        [ForeignKey(nameof(PersonId))]
        public Person Person { get; set; }

        [ForeignKey(nameof(RoleId))]
        public Role Role { get; set; }


    }
}
