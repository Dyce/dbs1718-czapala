﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbs1718_czapala.Model
{
    public class MovieViewTable
    {
        public int MovieId { get; set; }
        public string MoveName { get; set; }
        public DateTime? Released { get; set; }
        public int? Runtime { get; set; }
        public string Summary { get; set; }
        public string ProductionCompany { get; set; }
        public int Comments { get; set; }
    }
}
